import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        await knex.schema.createTable('users',(table)=>{
            table.increments('id').primary();
            table.string('username', 50).notNullable().unique();
            table.string('email', 255).notNullable().unique();
            table.string('password', 255).notNullable();
            table.string('phone_number', 8);
            table.string('role').notNullable().comment('VALUE STUDIO OR STUDENT');
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('users');
}

