import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('booking');
    if(!hasTable){
        await knex.schema.createTable('booking',(table)=>{
            table.increments('id').primary();
            table.integer('post_id');
            table.foreign('post_id').references('post.id');
            table.integer('student_id');
            table.foreign('student_id').references('student.id');
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('booking');
}

