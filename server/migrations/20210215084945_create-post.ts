import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('post');
    if(!hasTable){
        await knex.schema.createTable('post',(table)=>{
            table.increments('id').primary();
            table.string('address', 255).notNullable();
            table.date('date');
            table.time('starting_time');
            table.time('ending_time');
            table.string('contact_number', 8);
            table.integer('number_of_student');
            table.integer('price');
            table.text('description');
            table.integer('created_by');
            table.foreign('created_by').references('studio.id');
            table.timestamps(false, true);
        });
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('post');
}

