CREATE DATABASE "c13_project_1"; "c13-project_1"

 -- \c c13-project-1 
 -- FOREIGN KEY (user_id)REFERENCES user(id),
 --

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    phone_number VARCHAR (8),
    role VARCHAR (255) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    photo VARCHAR(255)
    
);
COMMENT ON COLUMN users.role IS 'VALUE STUDIO OR STUDENT';


/*trainers change to studio */
CREATE TABLE studio(
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    photo VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE student(
    id SERIAL PRIMARY KEY,
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES users(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE post(
    id SERIAL PRIMARY KEY,
    address VARCHAR(255) NOT NULL,
    date DATE,
    starting_time TIME,
    ending_time TIME,
    contact_number VARCHAR(8),
    number_of_student INTEGER,
    price INTEGER,
    description TEXT,
    created_by INTEGER,
    FOREIGN KEY (created_by) REFERENCES studio(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE booking(
    id SERIAL PRIMARY KEY,
    post_id INTEGER, 
    FOREIGN KEY (post_id) REFERENCES post(id),
    student_id INTEGER,
    FOREIGN KEY (student_id)REFERENCES student(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
    SELECT post FROM student
    INNER J

ALTER TABLE post ADD COLUMN created_by integer;

ALTER TABLE post
ADD CONSTRAINT “post_created_by_fkey”
FOREIGN KEY (created_by)
REFERENCES studio(id);


INSERT INTO users (username, email, password, phone_number, role) values 
('Alan Lee', 'alanlee@gmail.com', '$2a$10$68mWAQFU2z3i5u43S5SY4uds1pCV4mC4nKrl5eYGuLZ.o2aGpnXFq', 66892616, 'studio'),
('Helen Chan', 'helenchan@gmail.com', '$2a$10$xzG5bnC11YdPJv9x3lveIunI.HgtMXrdiBHv4eaDWOYtay3URUsuW', 98372652, 'studio'),
('Wilson Leung', 'wilsonleung@gmail.com', '$2a$10$YMio0aWltIzu0dG51Un0BuecMoNXBr.sbOhq6cEuxAl38hZbNTIGy', 69274619, 'studio'),
('Danny Hung', 'dannyhung@gmail.com', '$2a$10$5I00MXHCf6ap9uCsFE1oZuwBq1iRgCvdrbqPueUUQLjN3V4Qp3Hf.', 64741422, 'studio'),
('Anthony Wong', 'anthonywong@gmail.com', '$2a$10$B8snpy1EHw4rKJ4BJAKtIOTCQ2c14v41w4JY7CRl1ytpJidNiRqky', 97652134, 'studio'),
('SiuMing', 'siuming@gmail.com', '$2a$10$nKZDEIuChAyB5cr0T6dcDuJFR9KNc6kKLVOXe89WDPeEZiBa30zCu', 65346854, 'student'),
('DaiMing', 'daiming@gmail.com', '$2a$10$MyfnCToyfeLFgBem5W71X.k8bAoPKNjMdMUxNNPMLCHEflBJzerUC', 62553355, 'student'),
('Hotgirl', 'hotgirl@gmail.com', '$2a$10$wMgD1h70Xgmk8nKri0jf0.fSFwyAnJ5E1nDJ7h6JTLcxhM/8Y2fDq', 93533852, 'student'),
('Handsomeboy', 'handsomeboy@gmail.com', '$2a$10$jeedzmcJvrTMYj1cYZ3qGeC2bfJ98lCN22OjkIkuq3gyvSrwG0z0m', 61159181, 'student'),
('Catgirl', 'catgirl@gmail.com', '$2a$10$i9pmvlVZIeV/d7W7Fi9/fOC1btBkkpvKWotRHA6uBgAj1/Ibmns1G', 96216349, 'student'),
('Ethen Li', 'ethenli@gmail.com', '$2a$10$R6h3.MHJu0daGH6wX1X4.unlJpROlwRrLH18Ijvo7MVlLlzbzlNBe', 65224468, 'student'),
('Kat Wong', 'katwong@gmail.com', '$2a$10$yXtkwle6wm4ToqUlD/zrT.UsYb0Mag9Cbzzb4FvTOpNy0.OC847Te', 91988679, 'student'),
('Paul Lee', 'paullee@gmail.com', '$2a$10$Dzp5AGVc8nP3b6dRHsbIVOSu.HGP74gMv33U9PxpI65KGUFE/d/0e', 94564238, 'student'),
('Yee Ho', 'yeeho@gmail.com', '$2a$10$rUHcobM5XIaEwMOj5PIKDuYaScBHDrLUAyW3Jk6lhze22YKzNOtgS', 67918842, 'student'),
('Queen Law', 'queenlaw@gmail.com', '$2a$10$oynWJydBwg5Kc3AICb9VVuLHAx9W9HjZyAw65fjFFpcepFdrWnj3e', 67859852, 'student');

INSERT INTO studio ( user_id ) values (1),(2),(3),(4),(5);

INSERT INTO student ( user_id ) values (6),(7),(8),(9),(10),(11),(12),(13),(14),(15);

INSERT INTO post (address, date, starting_time, ending_time, contact_number, number_of_student, price, description, created_by) values
('TWW, 112 Chuen Lung St', '2021-1-30', '14:30:00', '15:15:00', 66892616, 10, 200, 'There has heavy trainning equipment.', 1),
('KC, 13 Lai Fong St', '2021-2-4', '15:00:00', '15:45:00', 98372652, 15, 250, 'There has professional running equipment.', 2),
('CSW, Peninsula Tower 538 Castle Peak Rd', '2021-2-5', '17:00:00', '17:45:00', 66892616, 12, 240, '24-hours open', 1),
('Kai Seng Commercial Centre', '2021-2-2', '14:00:00', '14:45:00', 69274619, 10, 210, 'Different trainning eq', 3),
('Times Square', '2021-2-3', '18:00:00', '19:00:00', 64741422, 10, 300, 'Booking everyday if you want', 4),
('MegaBox', '2021-1-28', '15:00:00', '15:45:00', 98372652, 4, 300, 'Train your muscle!', 2),
('KT, Hung To Rd, Hung Mou Industrial Building', '2021-2-10', '17:00:00', '18:00:00', 97652134, 10, 200, 'Keep dirt!', 5),
('TKO, Nan Fung Plaza 3/F', '2021-2-9', '18:00:00', '19:00:00', 98372652, 20, 250, 'Learn some move to care body', 2),
('ST, Shatin Industrial Center', '2021-2-6', '18:00:00', '18:45:00', 69274619, 15, 200, 'Know about your body', 3);

INSERT INTO booking (post_id, student_id) values 
(1, 1),(2, 2),(2, 3),(3, 4),(3, 5),(4, 6),(4, 7),(5, 8),(5, 9),(6, 10),
(6, 1),(7, 2),(7, 3),(8, 4),(8, 5),(1, 6),(2, 7),(3, 8),(4, 9),(5, 10);