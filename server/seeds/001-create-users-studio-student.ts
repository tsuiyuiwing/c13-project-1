import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("student").del();
    await knex("studio").del();
    await knex("users").del();

    // Inserts seed entries
    await knex.insert([
        {
            username: "Alan Lee",
            email: "alanlee@gmail.com",
            password: "$2a$10$68mWAQFU2z3i5u43S5SY4uds1pCV4mC4nKrl5eYGuLZ.o2aGpnXFq",
            phone_number: 66892616,
            role: "studio"
        }, {
            username: "Helen Chan",
            email: "helenchan@gmail.com",
            password: "$2a$10$xzG5bnC11YdPJv9x3lveIunI.HgtMXrdiBHv4eaDWOYtay3URUsuW",
            phone_number: 98372652,
            role: "studio"
        }, {
            username: "Wilson Leung",
            email: "wilsonleung@gmail.com",
            password: "$2a$10$YMio0aWltIzu0dG51Un0BuecMoNXBr.sbOhq6cEuxAl38hZbNTIGy",
            phone_number: 69274619,
            role: "studio"
        }, {
            username: "SiuMing",
            email: "siuming@gmail.com",
            password: "$2a$10$nKZDEIuChAyB5cr0T6dcDuJFR9KNc6kKLVOXe89WDPeEZiBa30zCu",
            phone_number: 65346854,
            role: "student"
        }, {
            username: "DaiMing",
            email: "daiming@gmail.com",
            password: "$2a$10$MyfnCToyfeLFgBem5W71X.k8bAoPKNjMdMUxNNPMLCHEflBJzerUC",
            phone_number: 62553355,
            role: "student"
        }, {
            username: "Hotgirl",
            email: "hotgirl@gmail.com",
            password: "$2a$10$wMgD1h70Xgmk8nKri0jf0.fSFwyAnJ5E1nDJ7h6JTLcxhM/8Y2fDq",
            phone_number: 93533852,
            role: "student"
        }, {
            username: "Catgirl",
            email: "catgirl@gmail.com",
            password: "$2a$10$i9pmvlVZIeV/d7W7Fi9/fOC1btBkkpvKWotRHA6uBgAj1/Ibmns1G",
            phone_number: 96216349,
            role: "student"
        }, {
            username: "Paul Lee",
            email: "paullee@gmail.com",
            password: "$2a$10$Dzp5AGVc8nP3b6dRHsbIVOSu.HGP74gMv33U9PxpI65KGUFE/d/0e",
            phone_number: 94564238,
            role: "student"
        }
    ]).into('users');

    await knex("studio").insert([
        { user_id: 1 },
        { user_id: 2 },
        { user_id: 3 }
    ]);
    await knex("student").insert([
        { user_id: 4 },
        { user_id: 5 },
        { user_id: 6 },
        { user_id: 7 },
        { user_id: 8 }
    ]);
};
