import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("post").del();

    // Inserts seed entries
    await knex("post").insert([
        { 
            address: 'TWW, 112 Chuen Lung St', 
            date: "2021-1-30",
            starting_time: '14:30:00',
            ending_time: '15:15:00',
            contact_number: 66892616,
            number_of_student: 10,
            price: 200,
            description: 'There has heavy trainning equipment.',
            created_by: 1
        },{
            address: 'KC, 13 Lai Fong St', 
            date: "2021-2-4",
            starting_time: '15:00:00',
            ending_time: '15:45:00',
            contact_number: 98372652,
            number_of_student: 15,
            price: 250,
            description: 'There has professional running equipment.',
            created_by: 2
        },{
            address: 'CSW, Peninsula Tower 538 Castle Peak Rd', 
            date: "2021-2-20",
            starting_time: '17:00:00',
            ending_time: '17:45:00',
            contact_number: 66892616,
            number_of_student: 12,
            price: 240,
            description: '24-hours open',
            created_by: 1
        },{
            address: 'Kai Seng Commercial Centre', 
            date: "2021-2-25",
            starting_time: '14:00:00',
            ending_time: '14:45:00',
            contact_number: 69274619,
            number_of_student: 10,
            price: 210,
            description: 'Different trainning eq',
            created_by: 3
        },{
            address: 'Times Square', 
            date: "2021-3-15",
            starting_time: '18:00:00',
            ending_time: '19:00:00',
            contact_number: 69274619,
            number_of_student: 10,
            price: 300,
            description: 'Booking everyday if you want',
            created_by: 3
        },{
            address: 'MegaBox', 
            date: "2021-3-5",
            starting_time: '15:00:00',
            ending_time: '15:45:00',
            contact_number: 98372652,
            number_of_student: 4,
            price: 300,
            description: 'Train your muscle!',
            created_by: 2
        }
    ]);
};
