import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("booking").del();

    // Inserts seed entries
    await knex("booking").insert([
        { post_id: 1, student_id: 1 },
        { post_id: 2, student_id: 1 },
        { post_id: 3, student_id: 1 },
        { post_id: 4, student_id: 1 },
        { post_id: 5, student_id: 1 },
        { post_id: 6, student_id: 2 },
        { post_id: 1, student_id: 2 },
        { post_id: 2, student_id: 3 },
        { post_id: 3, student_id: 2 },
        { post_id: 4, student_id: 3 },
        { post_id: 5, student_id: 2 },
        { post_id: 6, student_id: 3 }
    ]);
};
