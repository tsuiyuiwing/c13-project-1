window.onload = () => {
    loadAndDisplayAllPost();
    loadAndDisplayAllUserInfo();
    loadAndDisplayForGreeting();
}
// for load the data from database , the 
async function loadAndDisplayAllPost() {
    const res = await fetch('/posts')

    const data = await res.json();
    console.log(data)

    const htmlStr = document.querySelector('.list-group')
    console.log(htmlStr, "hi")
    for (const post of data.data) {
        console.log(post)
        htmlStr.innerHTML += `
        <a href="#" class="list-group-item list-group-item-action" aria-current="true">
        <div onclick="location.href='/studio_booking_edit.html?id=${post.id}';" style="cursor: pointer;">
        <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">Starting Time：${post.starting_time}</h5></div>
        <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">End Time：${post.ending_time}</h5></div>
        <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">Address ：${post.address}</h5></div>
      </a>`
    }
}
// update DOM

async function loadAndDisplayAllUserInfo() {
    const res = await fetch('/users')

    const data = await res.json();
    console.log(data, "hihi")

    const htmlStr = document.querySelector('#user-profile')
    console.log(htmlStr, "hi")
    //for (const user of data.data){
    document.querySelector('#username').value = `${data.data.username}`;
    document.querySelector('#email').value = `${data.data.email}`;
    document.querySelector('#phone_number').value = `${data.data.phone_number}`;
    document.querySelector('#role').value = `${data.data.role}`;
    // }
}

async function loadAndDisplayForGreeting() {
    const res = await fetch('/users')

    const data = await res.json();
    console.log(data, "hihi2")

    const htmlStr = document.querySelector('#greeting')
    console.log(htmlStr, "hi")
    //for (const user of data.data){
    document.querySelector('#greeting').innerHTML = `User name: ${data.data.username}`;
    console.log(data.data.username)
    // }
}

