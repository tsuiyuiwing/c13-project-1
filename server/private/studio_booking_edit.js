

window.onload = () => {
    loadAndDisplayPostInfo();
    loadAndDisplayAllBookedStudent();
    loadAndDisplayForGreeting();
}

async function loadAndDisplayPostInfo() {
    const paramSearch = window.location.search;
    const searchParams = new URLSearchParams(paramSearch);
    const post_id = searchParams.get("id");
    const res = await fetch(`/post/${post_id}`);
    let data = await res.json();
    console.log(data);
    data.date = data.date.slice(0,10);
    let postInfo = `
                <form id="fill-in" >

                <div class="form-group">
                    <label for="validationCustom01">Instructor Name</label>
                    <input type="text" class="form-control" id="validationCustom01" placeholder="Name" value="${data.created_by}" required>
                </div>- 

                <div class="form-group">
                    <label for="address">Address</label>
                    <input name="address" class="form-control" type="text" value="${data.address}" id="address" required>
                </div>

                <div class="form-group">
                    <label for="date" >Date</label>
                      <input name="date" class="form-control" type="date" value="${data.date}" id="date">
                </div>

                <div class="form-group">
                    <label for="start-time">Start Time</label>
                    <input name="starting_time" class="form-control" type="time" value="${data.starting_time}" id="start-time" required >
                </div>

                <div class="form-group">
                    <label for="end-time">End Time</label>
                    <input name="ending_time" class="form-control" type="time" value="${data.ending_time}" id="end-time" required >
                </div>

                <div class="form-group">
                    <label for="tel-input">Contact Phone</label>
                    <input name="contact_number" class="form-control" type="tel" value="${data.contact_number}" id="tel-input" required >
                </div>

                <div class="form-group">
                    <label for="student-number">Student Number</label>
                    <input name="number_of_student" class="form-control" type="number" value="${data.number_of_student}" id="student-number" required >
                </div>

                <div class="form-group">
                    <label for="credit">Credit</label>
                    <input name="price" class="form-control" type="number" value="${data.price}" id="credit" required >
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea name="description" class="form-control" placeholder="${data.description}" id="description" rows="3" required ></textarea>
                </div>

                <button type="submit"> update post </button>

            </form>
        `;

    document.querySelector(".form.panel").innerHTML += postInfo;
    console.log(`student page is loaded.`);




    let form = document.getElementById('fill-in');
    form.addEventListener("submit", async(event)=>{
        event.preventDefault();
        const formobject = {};
        formobject["address"] = form.address.value;
        formobject["date"] = form.date.value;
        formobject["starting_time"] = form.starting_time.value;
        formobject["ending_time"] = form.ending_time.value;
        formobject["contact_number"] = form.contact_number.value;
        formobject["number_of_student"] = form.number_of_student.value;
        formobject["price"] = form.price.value;
        formobject["description"] = form.description.value;
        const res = await fetch (`/post/${post_id}`, {
            method: "put",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formobject),

        });
        if (res.status === 200) {
            window.location = '/studio_detail.html';
        }
})
}


async function loadAndDisplayAllBookedStudent(){
    const paramSearch = window.location.search
    const searchParams = new URLSearchParams(paramSearch)
    const post_id = searchParams.get("id")
    const searchStudent = await fetch(`/post/book/${post_id}`);
    let searchData = await searchStudent.json();
    console.log(searchData);
    let studentInHtml = ``;

    for (const bookStudentInfo of searchData.data){
      studentInHtml += `
      <div class="d-flex w-100 justify-content-between columnDiv">
        <table>
        <tr>
        <td><h5 class="mb-1">Username：</h5></td><td>${bookStudentInfo.username}</td>
        </tr>
        <tr>
        <td><h5 class="mb-1">Email：</h5></td><td>${bookStudentInfo.email}</td>
        </tr><tr>
        <td><h5 class="mb-1">Phone Number ：</h5></td><td>${bookStudentInfo.phone_number}</td>
        </tr>
        </table>
        </div>
        <hr>
        `
    }

    document.querySelector(".bookedStudent").innerHTML += studentInHtml;

}

async function loadAndDisplayForGreeting() {
    const res = await fetch('/users')

    const data = await res.json();
    //console.log(data, "hihi2")

    const htmlStr = document.querySelector('#greeting')
    //console.log(htmlStr, "hi")
    //for (const user of data.data){
    document.querySelector('#greeting').innerHTML = `User name: ${data.data.username}`;
    //console.log(data.data.username)
    // }
}