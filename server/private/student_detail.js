window.onload = () => {
    loadAndDisplayBookInfo();
    loadAndDisplayUserInfo();
    studentBookingValue();
    loadAndDisplayForGreeting();
    pastStudentBookingValue();
}

async function loadAndDisplayBookInfo() {
    // console.log('1 for book');
    const res = await fetch("/currectBooks"); //link bookRoutes.ts
    const data = await res.json();
    console.log(data);
    let currBooking = ``;
    for (const currBook of data.data) {
        let cutDate = (currBook.date).toString().split('T')[0] + ' ' + currBook.starting_time;
        // console.log(cutDate);
        if (Date.parse(currBook.now) < Date.parse(cutDate)) {
            currBooking += `
            <div class="card cardCSS" style="width: 18rem;" onclick="studentBookingCourse(${currBook.id})">
                <div class="card-body">
                    <h6 class="card-title"><b>Address:<br> ${currBook.address}</b></h6>
                    <h6 class="card-subtitle mb-2 text-muted">Course Time:<br>`+cutDate+`-${currBook.ending_time}</h6>
                    <label class="card-text">Contact:<br>${currBook.contact_number}</label><br>
                    <label class="card-text">Number of student:${currBook.number_of_student}</label><br>
                    <label class="card-text">Price:${currBook.price}</label><br>
                    <p class="card-text">Description:<br>${currBook.description}</p>
                </div>
            </div>
        `;
        }
    }//<button onclick="studentBookingCourse(${currBook.id})">Book this course</button>
    document.querySelector(".currBookingValue").innerHTML = currBooking;
    // console.log(`student page is loaded.`);

}

async function loadAndDisplayUserInfo() {
    
    // console.log('2 for user');
    const res = await fetch("/currectStudent"); //link bookRoutes.ts
    const data = await res.json();
    //console.log(data);
    // console.log(`user id : ${data.data.id}`);
    document.querySelector("#student_name").value = data.data.username;
    document.querySelector("#student_contact").value = data.data.phone_number;
    document.querySelector("#student_email").value = data.data.email;

}

async function studentBookingCourse(courseID) {//link by loadAndDisplayBookInfo()
    const res = await fetch(`/bookingCourse/${courseID}`, {
        method: "POST"
    });
    const data = await res.json();



    window.alert('Success to booking this course!!');
    // console.log(data);
    if (res.status === 200) {
        window.location = "/student_detail.html";
    } else {
        const data = await res.json();
        alert(data.message);
    }

}

async function studentBookingValue() {
    const res = await fetch("/studentBookingValue"); //link bookRoutes.ts
    const data = await res.json();
    console.log(data.data);
    let currBooking = ``;
    

    for (const currBook of data.data) {
        let cutDate = (currBook.date).toString().split('T')[0] + ' ' + currBook.starting_time;
        console.log(Date.now());
        if (Date.parse(currBook.now) < Date.parse(cutDate)) {
            currBooking += `
            <div class="card" style="width: 18rem;" data-id="${currBook.id}">
                <div class="card-body">
                    <h5 class="card-title">Address: ${currBook.address}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Course Time:<br>`+cutDate+`-${currBook.ending_time}</h6>
                    <label class="card-text">Contact:<br>${currBook.contact_number}</label><br>
                    <label class="card-text">Number of student:${currBook.number_of_student}</label><br>
                    <label class="card-text">Price:${currBook.price}</label><br>
                    <p class="card-text">Description:${currBook.description}</p>
                    <button onclick="cancelBooking(${currBook.id})">Cancel course</button>
                </div>
            </div>
        `;
        }
    }
    document.querySelector(".studentBookingValue").innerHTML = currBooking;
    // console.log(`student page is loaded.`);
}

async function loadAndDisplayForGreeting() {
    const res = await fetch('/users')

    const data = await res.json();
    // console.log(data)
    // console.log("hihi")

    const htmlStr = document.querySelector('#greeting')
    // console.log(htmlStr, "hi")
    document.querySelector('#greeting').innerHTML = `User name: ${data.data.username}`;
    // console.log(data.data.username)
}

async function pastStudentBookingValue(){
    const res = await fetch("/studentBookingValue");
    const data = await res.json();
    // console.log(data);
    let pastBooking = ``;

    for (const currBook of data.data) {
        let cutDate = (currBook.date).toString().split('T')[0] + ' ' + currBook.starting_time;
        if (Date.parse(currBook.now) > Date.parse(cutDate)) {
            pastBooking += `
            <div class="card" style="width: 18rem;" data-id="${currBook.id}">
                <div class="card-body">
                    <h5 class="card-title">Address: ${currBook.address}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Course Time:<br>`+cutDate+`-${currBook.ending_time}</h6>
                    <label class="card-text">Contact:<br>${currBook.contact_number}</label><br>
                    <label class="card-text">Number of student:${currBook.number_of_student}</label><br>
                    <label class="card-text">Price:${currBook.price}</label><br>
                    <p class="card-text">Description:${currBook.description}</p>
                </div>
            </div>
        `;
        }
    }
    document.querySelector(".pastStudentBookingValue").innerHTML = pastBooking;
    // console.log(`student page is loaded.`);
}

async function cancelBooking(currBook_ID){
    const res = await fetch (`/cancelBooking/${currBook_ID}`, {
        method: "POST"
    });
    const data = res.json();
    console.log(data);

    
    if(res.status == 200){
        window.alert(`Your have cancel the class!`);
        window.location = "/student_detail.html";
    } else {
        const errMessage = data.message;
        console.log(errMessage);
    }
}