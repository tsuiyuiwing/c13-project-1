import express from "express";
import { userController } from "../main";


export const userRoutes = express.Router();

userRoutes.post("/users", userController.createUser);
userRoutes.post("/login", userController.login);
userRoutes.get("/logout", userController.logout);
userRoutes.get("/currectStudent", userController.getStudent);

