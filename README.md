- clone key (SSH)
```
git@gitlab.com:tsuiyuiwing/c13-project-1.git
```
- hardcode file path
```
tsuiyuiwing/c13-project-1
```
- **sudo service**
```
sudo service postgresql start
sudo su postgres
sudo -U <username> -W -h <hostname> <DBname>
```
- **Main add(yarn)**
```
sudo npm install -g yarn
```
```
yarn add --dev ts-node typescript @types/node
yarn add --dev jest
yarn add --dev typescript ts-jest @types/jest
yarn ts-jest config:init
```
- [ ]BAD003
```
yarn add knex @types/knex pg @types/pg
```
- [ ]knexfile.ts
```
yarn knex init -x ts
```
- environment change (default development)
```
knex --env production <any-command>
```
- **knex migrate & seed setting**
```
yarn knex migrate:make create-users
yarn knex migrate:make create-studio
yarn knex migrate:make create-student
yarn knex migrate:make create-post
yarn knex migrate:make create-booking
yarn knex seed:make -x ts create-users-studio-student
yarn knex seed:make -x ts create-post
yarn knex seed:make -x ts create-booking
```
- **Knex migrate & seed running**
```
yarn knex migrate:latest
yarn knex migrate:up
yarn knex migrate:rollback
yarn knex migrate:down
-----------------------------
yarn knex seed:run
```